// Required imports
const { ApiPromise, WsProvider } = require('@polkadot/api');
const promClient = require('prom-client')
const http = require('http')
const url = require('url')
const fs = require('fs');
const yaml = require('js-yaml');

require('log-timestamp');

async function initApi(providerAddress) {
  provider = new WsProvider(providerAddress);
  return await ApiPromise.create({ provider });
}

function setupPromClient(){
  // Create a Registry which registers the metrics
  const register = new promClient.Registry();

  // Define the HTTP server
  const server = http.createServer(async (req, res) => {
    // Retrieve route from request object
    const route = url.parse(req.url).pathname;

    if (route === '/metrics') {
      // Return all metrics the Prometheus exposition format
      console.log('Got request from : ' + req.socket.remoteAddress);
      res.setHeader('Content-Type', register.contentType);
      res.end(await register.metrics());
    }
  })

    return [register, server];
}

function createProducedBlocksGauge(register){
  return new promClient.Gauge({
    name: 'producedBlocks',
    help: 'Number of produced blocks',
    labelNames: ['validatorId'],
  });
}

function createIsActiveGauge(register){
  return new promClient.Gauge({
    name: 'isActive',
    help: 'Are validators active in current session => 1 means active',
    labelNames: ['validatorId'],
  });
}

function createHeartbeatsGauge(){
  return new promClient.Gauge({
    name: 'heartbeats',
    help: 'Heartbeats received => 1 means at least one received',
    labelNames: ['validatorId'],
  });
}

function createSomeOfflineGauge(register){
  return new promClient.Gauge({
    name: 'someOffline',
    help: 'Last session someOffline => 1 means validator was reported offline',
    labelNames: ['validatorId'],
  });
}

function createSessionBlockGauge(register){
  return new promClient.Gauge({
    name: 'sessionBlock',
    help: 'Current session block count'
  });
}

function createValidatorsSizeGauge(register){
  return new promClient.Gauge({
    name: 'validatorsSizeGauge',
    help: 'Current validators count'
  });
}

function enableServer(server, serverReady){
  if (serverReady === false) {
    server.listen(8080, '0.0.0.0');
    serverReady = true;
    console.log('Server ready and listening on port 8080');
  }
}

async function getActiveValidators(validatorsList, isActiveGauge, heartbeatsGauge, someOfflineGauge, validatorsSizeGauge) {
  let sessionValidators = await api.query.session.validators();
  validatorsSizeGauge.set({}, sessionValidators.length);
  let activeValidators = [];

  validatorsList.forEach((validatorId) => {
    let isActive = 0;
    if (sessionValidators.includes(validatorId)) {
      activeValidators.push(validatorId);
      isActive = 1;
    }
      isActiveGauge.set({"validatorId": validatorId}, isActive);
      someOfflineGauge.set({"validatorId": validatorId}, 0);

      if (isActive == 1) {
        heartbeatsGauge.set({"validatorId": validatorId}, 0);
      }
  });

  return activeValidators;
}

async function configureNewSession(promRegister, validatorsList, isActiveGauge, producedBlocksGauge, heartbeatsGauge, someOfflineGauge, sessionBlockGauge, validatorsSizeGauge) {
  promRegister.clear();

  promRegister.registerMetric(isActiveGauge);
  promRegister.registerMetric(producedBlocksGauge);
  promRegister.registerMetric(heartbeatsGauge);
  promRegister.registerMetric(someOfflineGauge);
  promRegister.registerMetric(sessionBlockGauge);
  promRegister.registerMetric(validatorsSizeGauge);

  let header = await api.rpc.chain.getHeader();
  let sessionStartBlock = parseInt(header.number);
  let activeValidators =  await getActiveValidators(validatorsList, isActiveGauge, heartbeatsGauge, someOfflineGauge, validatorsSizeGauge);

  return [sessionStartBlock, activeValidators];
}

async function updateValidatorMetrics(activeSessionValidators, producedBlocksGauge) {
  const currentSessionIndex = await api.query.session.currentIndex();
  let validatorsWithoutBlock = [];
  await Promise.all(activeSessionValidators.map(async validatorId => {
    let authoredBlocks = parseInt(await api.query.imOnline.authoredBlocks(currentSessionIndex, validatorId));
    producedBlocksGauge.set({"validatorId": validatorId}, authoredBlocks);
    if (authoredBlocks == 0) {
      validatorsWithoutBlock.push(validatorId)
    }
  }));
  console.log("Number of validators that have not produced a block in current session : " + validatorsWithoutBlock.length);
}

async function getSessionProgress(currentBlockNumber) {
  halfSessionBlockNumber = await api.query.imOnline.heartbeatAfter();
  return currentBlockNumber - parseInt(halfSessionBlockNumber);
}


async function main () {

  // Get validators file from args
  var validatorsFile = process.argv[2];
  var config = yaml.load(fs.readFileSync(validatorsFile, 'utf8'));
  var validatorsList = config.validators;
  var serveWhenReady = config.serveWhenReady;
  var providerAddress = config.provider;
  var debugMode = config.debugMode;
  var serverReady = false;
  var sessionKeys = new Map();
  var nextSessionKeys = new Map();
  var someOfflineList = [];

  if (debugMode){
    console.log("Debug mode")
  }

  // Init API
  api = await initApi(providerAddress);

  // Setup prometheus client
  const [promRegister, server] = setupPromClient();

  // Create gauges
  isActiveGauge = createIsActiveGauge(promRegister)
  producedBlocksGauge = createProducedBlocksGauge(promRegister);
  heartbeatsGauge = createHeartbeatsGauge(promRegister);
  someOfflineGauge = createSomeOfflineGauge(promRegister);
  validatorsSizeGauge = createValidatorsSizeGauge(promRegister);
  sessionBlockGauge = createSessionBlockGauge(promRegister);
  sessionBlockGauge.set({}, 0);

  if (!serveWhenReady) {
    server.listen(8080, '0.0.0.0');
    console.log('Server ready and listening on port 8080');
    serverReady = true;
  }

  // Get validators that are part of the current session
  var [sessionStartBlock, activeValidators] = await configureNewSession(promRegister, validatorsList, isActiveGauge, producedBlocksGauge, heartbeatsGauge, someOfflineGauge, sessionBlockGauge, validatorsSizeGauge);

  // Subscribe to system events
  api.query.system.events((events) => {
    events.forEach(async (record) => {
      const { event, phase } = record;

      // Heartbeat event
      if (api.events.imOnline.HeartbeatReceived.is(event) == true) {

        key = event["data"][0].toString();
        var validator;
        if (typeof sessionKeys.get(key) != 'undefined') {
          validator = sessionKeys.get(key).toString();
          heartbeatsGauge.set({"validatorId": validator}, 1);
        }
        console.log("Got heartbeat event from : " + key);
      }

      // SomeOffline event
      if (api.events.imOnline.SomeOffline.is(event)) {
          console.log("Got SomeOffline event: " + event.toString());
          event.data.forEach((validatorId) => {
            if (validatorsList.includes(validatorId.toString())){
              someOfflineList.push(validatorId.toString());
            }
          });
      }

      // New session event
      if (api.events.session.NewSession.is(event) == true) {
        enableServer(server, serverReady);
        console.log("Got NewSession event. New session index is: " + event.data[0].toString());
        [sessionStartBlock, activeValidators] = await configureNewSession(promRegister, validatorsList, isActiveGauge, producedBlocksGauge, heartbeatsGauge, someOfflineGauge, sessionBlockGauge, validatorsSizeGauge);
        sessionKeys = nextSessionKeys;

        // Publish metrics for someOffline event of the past session
        someOfflineList.forEach((validatorId) => {
          heartbeatsGauge.set({"validatorId": validatorId}, 1);
        });

        // Empty list
        someOfflineList = [];

      }
    });
  });

  // Subscribe to new heads
  await api.rpc.chain.subscribeNewHeads(async (header) => {
    console.log(`New block: ${header.number}`);
    console.log("Blocks produced in current session : " + (parseInt(header.number) - sessionStartBlock));
    await updateValidatorMetrics(activeValidators, producedBlocksGauge);
    blocksSinceHalfSession = await getSessionProgress(parseInt(header.number));

    sessionBlockGauge.set({}, header.number - sessionStartBlock);

    if (debugMode){
      console.log("Blocks difference to half session: " + blocksSinceHalfSession);
    }
    if (blocksSinceHalfSession > 285 && blocksSinceHalfSession < 290) {
      // We should be close to the end of session, populate next session keys map
      let queuedKeys = await api.query.session.queuedKeys();
      nextSessionKeys = new Map();
      if (debugMode){
        console.log("Populating next session keys");
      }
      queuedKeys.forEach(key => {
        if (validatorsList.includes(key[0].toString())) {
            key[1].forEach((sessionKey) => {
              nextSessionKeys.set(sessionKey.toString(), key[0].toString());
            });
        }
      });
    }
  });
}

main().catch(console.error);