This application monitors a list of given validatorIDs and exposes corresponding prometheus metrics.

Metrics exposed:
- isActive: indicates if the node is part of the current validators active set.
- producedBlocks: number of blocks produced per validator from the given validators list.
- heartbeats: indicates if any heartbeat was received per validator from the given validators list. Metric is only correctly populated once a new session event has been received.
- someOffline: indicates if any validator from the given validators list was flagged as offline.
- sessionBlock: shows current block in the session. Value is only correct once a new session event has been received.
- validatorsSize: Number of validators in the current active set.

The application uses a config file to define the kusama node address, the validatorIDs to monitor and the serveWhenReady flag. As the application required a new session event before exposing correct metrics, setting the serveWhenReady to true will make sure that the http service is not exposed before that event. Set it to false for debugging only.

Some dependencies are required. Install them with:
```
yarn install
```

Once done, the application can be started with:
```
node main.js resources/config.yaml
```

### TODOs
A few workarounds are currently used and would have to be fixed before deploying to production:
- Ideally, the application would be event based only. However fetching the block author after subscribing to new heads doesn't work. To fix this the application calls `api.query.imOnline.authoredBlocks` at each new block to check how many blocks have been produced by validators in the given list.
- Current session progress is only estimated and not fetched from the API.
- Heartbeats are sent by nodes using their session ImOnline key. Ideally, the matching between the session key and the validatorId should be done by calling `api.query.session.keyOwner` but I currently don't know which keyTypeId is required nor how the session key should be encoded. As a workaround, the application calls `api.query.session.queuedKeys();` when approaching the end of the session to get the keys for the next session. This is used to build a map of sessionKey => validatorId and is then used while in the next session to do the matching.
- Application unit testing
- Integration testing using charts and containers
- Use `api.query.imOnline.authoredBlocks` and `api.query.imOnline.receivedHeartbeats` at startup or on missed blocks event only. This would allow to have an accurate state without having to wait for a new session event. Furthermore, API calls would be lighter as complete authoredBlocks data wouldn't have to be retrieved at each new block.
