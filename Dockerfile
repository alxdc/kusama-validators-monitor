FROM node:14

WORKDIR /app

COPY src/package.json src/main.js ./

RUN yarn install

EXPOSE 8080

CMD ["node", "main.js", "/app/config/config.yaml"]